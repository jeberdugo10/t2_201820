package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface DoublyLinkedList<T> extends Iterable<T> {

	Integer getSize();
	void agregarFinal (Node<T> n);
	void agregarPrincipio (Node<T> n);
	void agregarDespues (Node<T> n, Node<T> n1);
	void agregarAntes (Node<T> n, Node<T> n1);
	void eliminar (Node<T> n);
}
