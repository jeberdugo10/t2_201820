package model.data_structures;

import java.util.Iterator;

public class ListaDoble<E> implements DoublyLinkedList<E> {

	private Node<E> primero;
    private Node<E> ultimo;
    private int size;
    
    public ListaDoble()
    {
    	size=0;
    }
    
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getSize() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void agregarFinal(Node<E> n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void agregarPrincipio(Node<E> n) {
		
		Node<E> tmp = new Node<E>(n);
        if(primero != null ) 
        {
        	primero.anterior = tmp;
        }
        primero = tmp;
        if(ultimo == null) { ultimo = tmp;}
        size++;
		// TODO Auto-generated method stub
		
	}

	@Override
	public void agregarDespues(Node<E> n, Node<E> n1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void eliminar(Node<E> n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void agregarAntes(Node<E> n, Node<E> n1) {
		// TODO Auto-generated method stub
		
	}

	public Node<E> getPrimero() {
		return primero;
	}

	public void setPrimero(Node<E> primero) {
		this.primero = primero;
	}

	public Node<E> getUltimo() {
		return ultimo;
	}

	public void setUltimo(Node<E> ultimo) {
		this.ultimo = ultimo;
	}

}
