package model.data_structures;

public class Node <E> {
	
	public Node<E> siguiente;
	public Node<E> anterior;
	
	
	private E item;
	
	
	public Node (Node<E> n) {
	siguiente = null;
	this.item = n;}
	
	public Node<E> getSiguiente() {
	return siguiente;}
	
	public void setSiguiente ( Node<E> next) {
	this.siguiente = next;}
	
	public Node<E> getAnterior() {
		return anterior;}
		
		public void setAnterior ( Node<E> ant) {
		this.siguiente = ant;}
	
	public E getItem(){
	return item;}
	
	public void setItem (E item) {
	this.item = item;}

}
